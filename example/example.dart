import 'dart:ffi';
import 'package:ffi/ffi.dart';
import 'package:glfw/glfw.dart';

void main() {
  initGlfw();

  glfwInit!();
  print('GLFW: ${glfwGetVersionString!().cast<Utf8>().toDartString()}');

  var window = glfwCreateWindow!(640, 480, 'Dart FFI + GLFW'.toNativeUtf8(),
      nullptr.cast(), nullptr.cast());

  while (glfwWindowShouldClose!(window) != GLFW_TRUE) {
    glfwSwapBuffers!(window);
    glfwPollEvents!();
  }
}
